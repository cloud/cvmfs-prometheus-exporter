import argparse
import logging as log
import os
import prometheus_client as prom
import random
import time

from flask import Flask, request
from flask_prometheus import monitor


app = Flask("CVMFS stats")


CVMFS_EXTENDED_ATTRIBUTES = {
    'expires': 'Shows the remaining life time of the '
               'mounted root file catalog in minutes.',
    'inode_max': 'Shows the highest possible inode with '
                 'the current set of loaded catalogs.',
    'maxfd': 'Shows the maximum number of file '
             'descriptors available to file system clients.',
    'ncleanup24': 'Shows the number of cache '
                  'cleanups in the last 24 hours.',
    'nclg': 'Shows the number of currently loaded nested catalogs.',
    'ndiropen': 'Shows the overall number of opened directories.',
    'ndownload': 'Shows the overall number '
                 'of downloaded files since mounting.',
    'nioerr': 'Shows the total number of I/O '
              'errors encoutered since mounting.',
    'nopen': 'Shows the overall number of open() calls since mounting.',
    'pid': 'Shows the process id of the CernVM-FS Fuse process.',
    'revision': 'Shows the file catalog revision of the mounted root catalog, '
                'an auto-increment counter increased on every repository '
                'publish.',
    'rx': 'Shows the overall amount of downloaded kilobytes.',
    'speed': 'Shows the average download speed.',
    'timeout': 'Shows the timeout for proxied connections in seconds.',
    'timeout_direct': 'Shows the timeout for direct connections in seconds.',
    'uptime': 'Shows the time passed since mounting in minutes.',
    'usedfd': 'Shows the number of file descriptors currently '
              'issued to file system clients.',
}


def exec_command(cmd, all_lines=False):
    try:
        log.debug("Command to execute: %s" % cmd)
        stream = os.popen(cmd)
        output = stream.readlines()
        log.debug("Command output: %s" % output)
        output = [l.strip() for l in output]
        if all_lines:
            return output
        return output[-1]
    except Exception as e:
        log.error("Failed to execute cmd: %s, error: %s" % (cmd, str(e)))


def get_cvmfs_repo_mount_point(repo):
    mountpoint = exec_command("cvmfs_talk  -i %s mountpoint" % repo)
    log.debug("mountpoint for repo %s found: %s" % (repo, mountpoint))
    return mountpoint


class CvmfsGauge(object):
    def __init__(self, name, description, sample_cmd, _type=int):
        super(CvmfsGauge, self).__init__()
        self.description = description
        self.name = name or "cvmfs_%s" % attribute_name
        self.gauge = prom.Gauge(self.name, description, ['cvmfs_repository'])
        self.sample_cmd = sample_cmd
        self.type = _type

    def _prepare_sample_cmd(self, repo, mountpoint):
        sample_cmd = self.sample_cmd.replace("<mountpoint>", mountpoint)
        sample_cmd = sample_cmd.replace("<repo_name>", repo)
        return sample_cmd

    def set_value(self, value, repo):
        log.debug("Sampled gauge: %s, value: %s, repo: %s." %
                  (self.name, value, repo))
        try:
            self.gauge.labels(cvmfs_repository=repo).set(self.type(value))
        except ValueError as e:
            # We got something that we don't like here.
            # Just reset the value to 0
            log.debug("Invalid value %s for gauge %s with type %s."
                      % (value, self.name, self.type.__name__))
            self.gauge.labels(cvmfs_repository=repo).set(self.type("0"))

    def sample(self, repo, mountpoint):
        log.debug("Sampling for metric %s started." % self.name)
        sample_cmd = self._prepare_sample_cmd(repo, mountpoint)
        value = exec_command(sample_cmd)
        self.set_value(value, repo)
        log.debug("Sampling for metric %s finished." % self.name)


class CvmfsAttributeGauge(CvmfsGauge):
    def __init__(self, attribute_name, description, name=None):
        name = name or "cvmfs_%s" % attribute_name
        self.attribute_name = attribute_name
        sample_cmd = "attr -g <attribute> <mountpoint>"
        super(CvmfsAttributeGauge, self).__init__(
            name, description, sample_cmd)

    def _prepare_sample_cmd(self, repo, mountpoint):
        sample_cmd = super(CvmfsAttributeGauge, self)._prepare_sample_cmd(
                repo, mountpoint)
        sample_cmd = sample_cmd.replace("<attribute>", self.attribute_name)
        return sample_cmd


class CvmfsGaugeCollection(object):
    def __init__(self):
        super(CvmfsGaugeCollection, self).__init__()
        self.reported_repos = list()
        self.gauges = list()

    def _append_metric(self, gauge):
        log.info("initialized metric: %s." % gauge.name)
        self.gauges.append(gauge)

    def add_gauge(self, name, description, sample_cmd, value_type=int):
        gauge = CvmfsGauge(name, description, sample_cmd, value_type)
        self._append_metric(gauge)

    def add_exetended_attribute_gauge(self, attribute_name, name=None):
        description = CVMFS_EXTENDED_ATTRIBUTES[attribute_name]
        gauge = CvmfsAttributeGauge(attribute_name, description)
        self._append_metric(gauge)

    def mountpoint_for_repo(self, repo):
        return get_cvmfs_repo_mount_point(repo)

    def sample(self):
        mounted_repos = discover_mounted_repos()
        for repo in self.reported_repos:
            if repo not in mounted_repos:
                log.info("Detected that repo %s was unmounted." % repo)
                self.reset_metrics_for_repo(repo)
        self.reported_repos = list()
        for repo in mounted_repos:
            self.reported_repos.append(repo)
            mountpoint = self.mountpoint_for_repo(repo)
            self._sample_for_repo(repo, mountpoint)

    def reset_metrics_for_repo(self, repo):
        log.info("Resetting metrics for unmounted repo %s" % repo)
        for gauge in self.gauges:
            gauge.set_value(0, repo)

    def _sample_for_repo(self, repo, mountpoint):
        log.debug("Sampling round for repo %s started." % repo)
        for gauge in self.gauges:
            try:
                gauge.sample(repo, mountpoint)
            except Exception as e:
                log.error("Error sampling %s for %s on mountpoint %s: %s."
                          % (gauge.name, repo, mountpoint, str(e)))
        log.debug("Sampling round for repo %s finished." % repo)


def discover_mounted_repos():
    """Returns a list of the mounted cvmfs repos"""
    repos = exec_command("cvmfs_config status | awk '{print $1}'",
                         all_lines=True)
    log.debug("Found %s mounted cvmfs repos: %s." % (len(repos), repos))
    return repos


def stat_collection_for_cvmfs():
    """Creates a collection of stats for a given cvmfs repo and returns it"""
    collection = CvmfsGaugeCollection()
    collection.add_gauge(
        'cvmfs_cached_bytes', 'CVMFS currently cached bytes',
        "cvmfs_talk -i <repo_name> cache size | awk '{print $6}' | cut -c 2-")
    collection.add_gauge(
        'cvmfs_pinned_bytes', 'CVMFS currently pinned bytes',
        "cvmfs_talk -i <repo_name> cache size | awk '{print $10}' | cut -c 2-")
    collection.add_gauge(
        'cvmfs_total_cache_size', 'CVMFS configured cache size in MB',
        "cvmfs_talk -i <repo_name> parameters | grep CVMFS_QUOTA_LIMIT | "
        "cut -f1 -d' ' | cut -f2 -d'='")
    collection.add_gauge(
        'cvmfs_cache_hit_rate', 'CVMFS cache hit rate (%)',
        "cvmfs_config stat  regtest.cern.ch | awk '{print $14}' | tail -1",
        value_type=float)
    for attribute in CVMFS_EXTENDED_ATTRIBUTES:
        collection.add_exetended_attribute_gauge(attribute)
    return collection


def start_prometheus_exporter(port, sampling_interval):

    collection = stat_collection_for_cvmfs()

    monitor(app, port=port)
    while True:
        log.info("Sampling round started.")
        collection.sample()
        time.sleep(sampling_interval)
        log.info("Sampling round finished.")


def parse_args():
    parser = argparse.ArgumentParser()
    log_levels = ['critical', 'error', 'warning', 'info', 'debug']
    parser.add_argument(
        "-p", "--port", type=int, default=-1,
        help="The port where metrics are published. Can be"
             "passed as the env variable $PORT (defaults to 8080).")
    parser.add_argument(
        "-s", "--sampling_interval", type=int, default=-1,
        help="The interval used for sampling in seconds. Can be passed as"
             "the env variable $SAMPLING_INTERVAL (defaults to 1)")
    parser.add_argument(
        "-l", "--log_level", type=str, choices=log_levels, default=None,
        help="choose output log level. Can be passed as the env variable"
             "$LOG_LEVEL (defaults to info).")
    args = parser.parse_args()
    if args.port == -1:
        args.port = int(os.getenv("PORT", 8080))
    if args.sampling_interval == -1:
        args.sampling_interval = int(os.getenv("SAMPLING_INTERVAL", 10))
    if not args.log_level:
        args.log_level = str(os.getenv("LOG_LEVEL", 'info'))
    return args.port, args.sampling_interval, args.log_level


if __name__ == '__main__':
    port, sampling_interval, log_level = parse_args()

    log.basicConfig(format='%(asctime)s - %(message)s',
                    level=getattr(log, log_level.upper()))
    start_prometheus_exporter(port=port, sampling_interval=sampling_interval)
