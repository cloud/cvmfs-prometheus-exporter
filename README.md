# CVMFS prometheus exporter

The exporter will automatically discover the mounted cvmfs repositories using the 'cvmfs_config status' command.
For all mounted repos the following stats will be published:
* cvmfs_total_cache_size
* cvmfs_cached_bytes
* cvmfs_pinned_bytes
* cvmfs_cache_hit_rate
* cvmfs_pinned_bytes
* cvmfs_expires
* cvmfs_inode_max
* cvmfs_maxfd
* cvmfs_ncleanup24
* cvmfs_nclg
* cvmfs_ndiropen
* cvmfs_ndownload
* cvmfs_nioerr
* cvmfs_nopen
* cvmfs_pid
* cvmfs_revision
* cvmfs_rx
* cvmfs_speed
* cvmfs_timeout
* cvmfs_timeout_direct
* cvmfs_uptime
* cvmfs_usedfd

The description of each attribute stat can be found here:
https://cvmfs.readthedocs.io/en/stable/cpt-details.html#getxattr

All stats are reported (labeled) per cvmfs repository, for example:

     HELP cvmfs_speed Shows the average download
     TYPE cvmfs_speed
     cvmfs_speed{cvmfs_repository="repo1.cern.ch"} 97.0
     cvmfs_speed{cvmfs_repository="repo2.cern.ch"} 13.0

## Configuring the exporter

 - port: The port where the stats are published can be passed directly to the script as --port (-p) <value> or by setting the $PORT env variable (defaults to 8080).

 - sampling interval: The sampling interval can be passed directly to the exporter as --sampling_interval (-s) <value_in_seconds> or by setting the $SAMPLING_INTERVAL env variable (defaults to 1).

 - log level: The verbosity of the exporter can be passed to the script as --log_level (-l) <value> (critical, error, warning, info, debug) or by setting the $LOG_LEVEL env variable (defaults to info).

## Grafana dashboard

The json definition of an example grafana dashboard is included in this repo.
