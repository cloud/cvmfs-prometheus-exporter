FROM gitlab-registry.cern.ch/cloud/atomic-system-containers/cvmfsplugin:v1.0.0
WORKDIR /app

COPY cvmfs_exporter.py /app
COPY requirements.txt /app

RUN curl -s https://bootstrap.pypa.io/get-pip.py |  python && cd /app && pip install -r requirements.txt
